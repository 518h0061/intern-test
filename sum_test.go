package MyBigNumber

import (
	"math/rand"
	"strconv"
	"testing"
)

var IS_TESTING bool = true

func TestSum(t *testing.T) {

	// adden1_list := []string{"0", "1", "0", "5", "2", "7", "10", "10", "15", "15", "15", "59", "70", "12", "25", "10", "80", "88", "802", "808", "8012", "888", "801009", "9242000024002"}
	// adden2_list := []string{"0", "0", "20", "5", "5", "3", "2", "10", "7", "15", "51", "95", "30", "18", "97", "20", "70", "88", "802", "808", "2018", "888", "20012", "924201002409692"}
	// result_list := []string{"0", "1", "20", "10", "7", "10", "12", "20", "22", "30", "66", "154", "100", "30", "122", "30", "150", "176", "1604", "1616", "10030", "1776", "821021", "933443002433694"}

	adden1_list, adden2_list, result_list := generateTest(10)

	for i := 0; i < len(adden1_list); i++ {
		sum := elementarySum(adden1_list[i], adden2_list[i], IS_TESTING)
		if sum != result_list[i] {
			t.Error(adden1_list[i], " + ", adden2_list[i], " = ", sum)
		}
	}
}

func generateTest(num int) ([]string, []string, []string) {
	min := 0
	max := 100000000
	var list1 []string
	var list2 []string
	var sumList []string
	for i := 0; i < num; i++ {
		rand1 := rand.Intn(max-min) + min
		rand2 := rand.Intn(max-min) + min
		list1 = append(list1, strconv.Itoa(rand1))
		list2 = append(list2, strconv.Itoa(rand2))
		sumList = append(sumList, strconv.Itoa(rand1+rand2))
	}
	return list1, list2, sumList
}
