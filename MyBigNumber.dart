class MyBigNumber {
  /// Returns the sum of [value1] and [value2].
  static String elementarySum(String value1, String value2) {
    String sum = '';
    int len = value1.length > value2.length ? value1.length : value2.length;
    int original_len = len;
    int spare = 0;
    print('${value1} + ${value2}\n');

    while (len > 0) {
      int adden1 = value1 == ''
          ? 0
          : int.parse(value1.trim().split('')[value1.length - 1]);
      int adden2 = value2 == ''
          ? 0
          : int.parse(value2.trim().split('')[value2.length - 1]);
      int currentSum = spare + adden1 + adden2;

      if (currentSum >= 10) {
        String last =
            currentSum.toString().split('')[currentSum.toString().length - 1];
        sum = last + sum;
        spare = 1;
      } else {
        sum = currentSum.toString() + sum;
        spare = 0;
      }
      value1 = value1 == '' ? '' : value1.substring(0, value1.length - 1);
      value2 = value2 == '' ? '' : value2.substring(0, value2.length - 1);
      len--;

      if (len == 0 && spare == 1) {
        sum = spare.toString() + sum;
      }

      print('Bước ${original_len - len}: ${adden1} + ${adden2} = ${adden1 + adden2}');
      if (spare == 1 && len != 0) {
        // trường hợp có dư
        print('Tổng = ${sum} (Nhớ ${spare})\n');
      }
      else if (spare == 1 && len == 0) {
        // trường hợp cộng số cuối cùng có dư
        print('Tổng = ${sum}\n');
      } else {
        print('Tổng = ${sum}\n');
      }
    }
    return sum;
  }
}

void main() {
  MyBigNumber.elementarySum('165832', '2456');
}
