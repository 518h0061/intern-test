package MyBigNumber

import (
	"log"
	"strconv"
	"strings"
)

func elementarySum(value1 string, value2 string, isTesting bool) string {

	var max_length int
	var finalSum string = ""
	var spare int = 0

	value1_list := strings.Split(value1, "")
	value2_list := strings.Split(value2, "")

	if len(value1_list) > len(value2_list) {
		max_length = len(value1_list)
	} else {
		max_length = len(value2_list)
	}
	if isTesting == false {
		log.Println(value1, " + ", value2)
	}

	for i := 0; i < max_length; i++ {
		var adden1 int
		var adden2 int

		if i < len(value1_list) {
			adden1, _ = strconv.Atoi(value1_list[len(value1_list)-1-i])
		} else {
			adden1 = 0
		}

		if i < len(value2_list) {
			adden2, _ = strconv.Atoi(value2_list[len(value2_list)-1-i])
		} else {
			adden2 = 0
		}
		currentSum := adden1 + adden2 + spare

		if currentSum < 10 {
			finalSum = strconv.Itoa(currentSum) + finalSum
			spare = 0
		} else if currentSum >= 10 && i+1 != max_length {
			finalSum = strconv.Itoa(currentSum-10) + finalSum
			spare = 1
		} else {
			finalSum = strconv.Itoa(currentSum) + finalSum
			spare = 0
		}

		// printing
		if isTesting == false {
			log.Println("Bước ", i+1, ": ", adden1, " + ", adden2)
			if spare == 1 && i != max_length+1 {
				log.Println("Tổng = ", finalSum, " (Nhớ 1)")
			} else if spare == 1 && i == max_length+1 {
				log.Println("Tổng = ", finalSum)
			} else {
				log.Println("Tổng = ", finalSum)
			}
			log.Println("")
		}

	}
	return finalSum
}
